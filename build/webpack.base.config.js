const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

// HtmlWebpackPlugin 会在打包后，自动生成一个html文件，并把打包生成的js自动引入到这个html文件中
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: {
		//  配置文件的入口 main 会变成打包后的名字，根据输出name键值
		main: path.resolve(__dirname, '../src/main.js'),
	},
	output: {
		// publicPath: 'cdn'  //可以配置cdn地址 
		// 打包文件的出口
		path: path.resolve(__dirname, '../dist'),
		// 生成的js文件名称
		filename: 'js/[name]_[hash:8].js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader'
			},
			{
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 2048,
          name: 'img/[name].[hash:8].[ext]'
        }
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'media/[name].[hash:8].[ext]'
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'fonts/[name].[hash:8].[ext]'
        }
      },
			{
				test: /\.(sass|scss)$/,
				use: [
					{
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: process.env.NODE_ENV === 'development',
            },
          },
					{
						loader: "css-loader", // 将 CSS 转化成 CommonJS 模块
						options: {
							importLoaders: 2,  // 识别在sass文件中的引用的sass文件，防止不通过sassloader直接走cssloader
							modules: true,
							localIdentName: '[local]--[hash:base64:8]'
						}
					},
					{
						loader: "sass-loader" // 将 Sass 编译成 CSS
					},
					'postcss-loader'
				]
			},
			{
				test: /\.css$/,
				use: [
					{
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: process.env.NODE_ENV === 'development',
            }
          },
					'css-loader',
					'postcss-loader'
				]
			}
		]
	},
	optimization: {
		// tree shaking 按需加载
		usedExports: true,
		// 代码分割
		splitChunks: {
			chunks: 'all', 
			cacheGroups: {  /*缓存组，对于引入的node_modules中的 全打包进vendors.js 自己写的打包进common.js */
        vendors: {
          test: /[\\/]node_modules[\\/]/,
					priority: -10, // 优先级
					filename: 'js/vendors.js'
        },
        default: {
          priority: -20,
					reuseExistingChunk: true, // 忽略已经打包过的
					filename: 'js/common.js'
        }
      }
		}
	},
	plugins: [
		new HtmlWebpackPlugin({
			// 指定一个模板文件，生成的html按照这个模板来
			template: path.resolve(__dirname, '../public/index.html')
		}),
		new MiniCssExtractPlugin({
			filename: 'css/[name].[contenthash:8].css',
			chunkFilename: 'css/[name].[contenthash:8].chunk.css'
		})
	]
}