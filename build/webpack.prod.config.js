const merge = require ('webpack-merge')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const config = require('./webpack.base.config')
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack')
const prodConfig = {
	mode: "production",
	devtool: "none",
	optimization: {
		minimizer: [
			new OptimizeCssAssetsPlugin({}),
			new UglifyJSPlugin()
		]
	},
	plugins: [
		new CleanWebpackPlugin(),
		new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    })
	]
}

module.exports = merge(config, prodConfig)